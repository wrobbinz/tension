module.exports = {
    'extends': 'airbnb',
    'parser': 'babel-eslint',
    'rules': {
        'semi': [2, 'never'],
        'react/jsx-filename-extension': [0, 'off'],
        'jsx-a11y/anchor-is-valid': [0, 'off'],
    },
    'env': {
        'browser': true,
    },
}
