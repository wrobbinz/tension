module.exports = {
    "extends": "airbnb-base",
    "rules": {
        "semi": [2, 'never'],
        "class-methods-use-this": "off",
    },
    "globals": {
        "use": true,
    },
}
